import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class SuccessPage extends StatefulWidget {

  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Успешно',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 40,
                  bottom: 40

                ),
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png'),
                    fit: BoxFit.cover
                  )
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text(
                  'Ресторан АО',
                  style: GoogleFonts.roboto(
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text(
                  '18 февр. | 19:30 | стол 10',
                  style: GoogleFonts.roboto(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5),
                padding: EdgeInsets.symmetric(vertical: 6,horizontal: 16),
                decoration: BoxDecoration(
                  color: Color(0xff00A6CA),
                  borderRadius: BorderRadius.circular(20)
                ),
                child: Text('Стол забронирован',style: GoogleFonts.roboto(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.white
                ),),
              ),
              Text('14 февр. 2021, 17:04',style: GoogleFonts.roboto(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Colors.grey
              ),),
              SizedBox(
                height: 92,
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 40,bottom: 48),
                child: ButtonTheme(
                  minWidth: 264,
                  height: 48,
                  child: RaisedButton(
                    onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>SuccessPage())),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      side: BorderSide(
                        color: Color(0xff00A6CA)
                      )
                    ),
                    child: Text(
                      'Отменить бронь',
                      style: GoogleFonts.roboto(
                          fontSize: 14, fontWeight: FontWeight.w400,color: Color(0xff00A6CA)),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
