import 'dart:convert';

import 'package:btime/main/items/OrganizationItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class OrganizationsScreen extends StatefulWidget {
  final categoryId;
  final categoryName;

  const OrganizationsScreen({Key key, this.categoryId, this.categoryName}) : super(key: key);
  @override
  _OrganizationsScreenState createState() => _OrganizationsScreenState();
}

class _OrganizationsScreenState extends State<OrganizationsScreen> {
  var client = http.Client();
  List<dynamic> organizations = [];
  void getOrganizations()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token =  prefs.get('token');
    final response = await client.get(Uri.parse('$endpoint/organizations?category_id=${this.widget.categoryId}'), headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"
    });
    print(response.body);

    if(response.statusCode == 200){
      final body = jsonDecode(response.body);
      setState(() {
        organizations = body['organizations'];
      });
    }

  }

  @override
  void initState() {
    // TODO: implement initState
    getOrganizations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${this.widget.categoryName}',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),

        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 25),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Поиск',
                    prefixIcon: Icon(
                      Icons.search,
                      color: Theme.of(context).secondaryHeaderColor,
                    ),
                    focusColor: Colors.black,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 32),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Популярные рестораны',
                      style: GoogleFonts.roboto(
                          fontSize: 16, fontWeight: FontWeight.w400),
                    ),
                    Text(
                      'Все',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 42),
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,

                  itemBuilder: (context, item) => OrganizationItem(organizations[item]),
                  itemCount: organizations.length,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
