import 'package:btime/main.dart';
import 'package:btime/main/reserve/Success.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ReserveScreen extends StatefulWidget {
  final Map<String,dynamic> reserve;

  const ReserveScreen({Key key, this.reserve}) : super(key: key);
  @override
  _ReserveScreenState createState() => _ReserveScreenState();
}

class _ReserveScreenState extends State<ReserveScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Успешно',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                    top: 40,
                    bottom: 40

                ),
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage('$fileEndPoint${this.widget.reserve['organization']['image']}'),
                        fit: BoxFit.cover
                    )
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text(
                  '${this.widget.reserve['organization']['name']}',
                  style: GoogleFonts.roboto(
                      fontSize: 20,
                      fontWeight: FontWeight.w600
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text(
                  '${DateTime.parse(this.widget.reserve['date']).day.toString().length != 1 ? DateTime.parse(this.widget.reserve['date']).day : '0${DateTime.parse(this.widget.reserve['date']).day}'}.'
                      '${DateTime.parse(this.widget.reserve['date']).month.toString().length != 1 ? DateTime.parse(this.widget.reserve['date']).month : '0${DateTime.parse(this.widget.reserve['date']).month}'}.${DateTime.parse(this.widget.reserve['date']).year} | ${this.widget.reserve['start_time']} | ${this.widget.reserve['position']['name']}',
                  style: GoogleFonts.roboto(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5),
                padding: EdgeInsets.symmetric(vertical: 6,horizontal: 16),
                decoration: BoxDecoration(
                    color: Color(0xff00A6CA),
                    borderRadius: BorderRadius.circular(20)
                ),
                child: Text('Стол забронирован',style: GoogleFonts.roboto(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white
                ),),
              ),
              SizedBox(
                height: 92,
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 40,bottom: 48),
                child: ButtonTheme(
                  minWidth: 264,
                  height: 48,
                  child: RaisedButton(
                    onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>SuccessPage())),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                        side: BorderSide(
                            color: Color(0xff00A6CA)
                        )
                    ),
                    child: Text(
                      'Отменить бронь',
                      style: GoogleFonts.roboto(
                          fontSize: 14, fontWeight: FontWeight.w400,color: Color(0xff00A6CA)),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }
}
