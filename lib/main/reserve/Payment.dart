import 'dart:convert';

import 'package:btime/main/reserve/Index.dart';
import 'package:btime/main/reserve/Success.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

class PaymentScreen extends StatefulWidget {
  final Map<String,dynamic> form;

  const PaymentScreen({Key key, this.form}) : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  Map<String,dynamic> form;

  TextEditingController _phoneController = MaskedTextController(mask: '+0 (000) 000-00-00',text: '+7');
  TextEditingController _cardNumber = MaskedTextController(mask: '0000 0000 0000 0000',text: '');
  TextEditingController _cvv = MaskedTextController(mask: '000',text: '');

  TextEditingController _dataCode = MaskedTextController(mask: '00/00',text: '');

  TextEditingController _nameController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      form = this.widget.form;
    });

    print(form);
    getUser();
    super.initState();

  }

  var client = http.Client();


  Map<String,dynamic> user;
  void getUser()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token =  prefs.get('token');

    final response = await client.get(Uri.parse('$endpoint/user'), headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"
    });

    if(response.statusCode == 200){
      final data = jsonDecode(response.body);
      setState(() {
        user = data['user'];
        _phoneController.text = user['phone'];
      });
    }

  }

  void sendAddReserveRequest()async{
    form['phone'] = _phoneController.text;
    form['name'] = _nameController.text;


    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');
    print(form);
    final response = await client.post(
        Uri.parse('$endpoint/reserves'),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        },
        body: form
    );

    print(response.statusCode);
    if(response.statusCode == 200){
      Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          builder: (BuildContext context) {
            return ReserveIndex();
          },
        ),
            (_) => false,
      );

    }


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Оплата брони',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Личная информация',
                style: GoogleFonts.roboto(
                    fontSize: 16, fontWeight: FontWeight.w600),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: TextField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    hintText: 'Фамилия и имя',

                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: TextField(
                  controller: _phoneController,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    hintText: 'Номер телефона',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                'Введите платежную информацию',
                style: GoogleFonts.roboto(
                    fontSize: 16, fontWeight: FontWeight.w600),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: TextField(
                  controller: _cardNumber,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'Номер карты',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: TextField(
                  controller: _dataCode,
                  decoration: InputDecoration(
                    hintText: 'Срок действия карты ',

                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: _cvv,
                  decoration: InputDecoration(
                    hintText: 'CVV',

                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 40, bottom: 48),
                child: ButtonTheme(
                  minWidth: 264,
                  height: 48,
                  child: RaisedButton(
                    onPressed: ()=>sendAddReserveRequest(),
                    color: Color(0xff00A6CA),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      'Оплатить',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
