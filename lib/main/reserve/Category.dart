import 'dart:convert';

import 'package:btime/auth/Login.dart';
import 'package:btime/main/items/CategoryItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  var client = http.Client();
  List<dynamic> categories = [];

  void getCategory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final token =  prefs.get('token');
    final response = await client.get(Uri.parse('$endpoint/categories'), headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"
    });

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        categories = data['categories'];
      });
    } else {
      print(response.body);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Встать в очередь',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: List.generate(categories.length, (index) {
          return CategoryItem(categories[index]);
        }),
      ),
      backgroundColor: Colors.white,
    );
  }
}
