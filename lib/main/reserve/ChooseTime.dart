import 'dart:convert';

import 'package:btime/main/reserve/Payment.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

class ChooseDateTimeScreen extends StatefulWidget {
  final organizationId;

  const ChooseDateTimeScreen({Key key, this.organizationId}) : super(key: key);

  @override
  _ChooseDateTimeScreenState createState() => _ChooseDateTimeScreenState();
}

class _ChooseDateTimeScreenState extends State<ChooseDateTimeScreen> {
  List<dynamic> positions = [];

  String dropdownValue = 'One';

  TextEditingController _hourController = TextEditingController();
  TextEditingController _minuteController = TextEditingController();
  var client = http.Client();

  Map<String,dynamic> form = {
    "organization_id": null,
    "date" : DateTime.now().toString(),
    "start_time": null,
    "position_id" : null
  };
  void getPositions() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');
    final response = await client.get(
        Uri.parse('$endpoint/positions/${this.widget.organizationId}'),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        });

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        positions = body['positions'];
      });
      print(positions);
    }
  }


  @override
  void initState() {
    getPositions();
    form['organization_id'] = this.widget.organizationId.toString();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Кафе',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              margin: EdgeInsets.only(top: 27),
              child: Row(
                children: [
                  Icon(
                    Icons.calendar_today,
                    color: Colors.black,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 11),
                    child: Text(
                      'Выбрать дату',
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20, bottom: 30),
              child: DatePicker(
                DateTime.now(),
                height: 80,
                dateTextStyle: GoogleFonts.roboto(
                    fontWeight: FontWeight.w400, fontSize: 14),
                monthTextStyle: GoogleFonts.roboto(
                    fontWeight: FontWeight.w400, fontSize: 12),
                initialSelectedDate: DateTime.now(),
                selectionColor: Color(0xff00A6CA),
                selectedTextColor: Colors.white,
                onDateChange: (date) {
                  // New date selected
                  setState(() {
                    form['date'] = date.toString();
                  });
                },
                locale: "ru_RU",
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Icon(
                    Icons.access_time,
                    color: Colors.black,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 11),
                    child: Text(
                      'Выбрать время',
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.only(top: 25),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    width: 100,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      controller: _hourController,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xffEBF3F4),
                        hintText: 'Часов',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Color(0xffEBF3F4))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Color(0xffEBF3F4))),
                      ),
                    ),
                  ),
                  Container(
                    width: 100,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      controller: _minuteController,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xffEBF3F4),
                        hintText: 'Минут',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Color(0xffEBF3F4))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Color(0xffEBF3F4))),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Row(
                children: [
                  Icon(
                    Icons.check,
                    color: Colors.black,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 11),
                    child: Text(
                      'Выбрать столик',
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 20,right: 20),
              width: 150,
              child: DropdownButtonFormField(
                  decoration: InputDecoration(
                    hintText: 'Выберите стол',
                    filled: true,
                    fillColor: Color(0xffEBF3F4),
                    errorStyle: TextStyle(color: Colors.yellow),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Color(0xffEBF3F4))),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide: BorderSide(color: Color(0xffEBF3F4))),
                  ),
                  onChanged: (value){
                    form['position_id'] = value.toString();
                  },
                  items: positions.map((map) {
                    return DropdownMenuItem(
                      child: Text(map['name']),
                      value: map['id'],
                    );
                  }).toList()),
            ),

            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 40, bottom: 48),
              child: ButtonTheme(
                minWidth: 264,
                height: 48,
                child: RaisedButton(
                  onPressed: ()  {
                    form['start_time'] = '${_hourController.text}:${_minuteController.text}';
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>PaymentScreen(form:form)));
                  },
                  color: Color(0xff00A6CA),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  child: Text(
                    'Забронировать столик',
                    style: GoogleFonts.roboto(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
