import 'dart:convert';

import 'package:btime/main/items/ReservationItem.dart';
import 'package:btime/main/reserve/Category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

class ReserveIndex extends StatefulWidget {
  @override
  _ReserveIndexState createState() => _ReserveIndexState();
}

class _ReserveIndexState extends State<ReserveIndex> {
  var client = http.Client();

  List<dynamic> reserves = [];

  void getReservations() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');

    final response = await client.get(Uri.parse('$endpoint/reserves'),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        });

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);

      setState(() {
        reserves = body['reserves'];
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getReservations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои брони',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        actions: [
          GestureDetector(
            onTap: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => Category())),
            child: Container(
              child: Icon(
                Icons.add,
                size: 35,
                color: Theme.of(context).secondaryHeaderColor,
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: reserves.length > 0
          ? Container(
              margin: EdgeInsets.all(20),
              child: ListView.builder(
                  itemBuilder: (context, index) =>
                      ReservationItem(reserves[index]),itemCount: reserves.length,))
          : RefreshIndicator(
              onRefresh: () async {
                await getReservations();
              },
              child: GestureDetector(
                onTap: () => getReservations(),
                child: Center(
                  child: Text('Список пуст'),
                ),
              ),
            ),
    );
  }
}
