import 'dart:convert';

import 'package:btime/main.dart';
import 'package:btime/main/reserve/ChooseTime.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

class OrganizationScreen extends StatefulWidget {
  final Map<String, dynamic> organization;

  const OrganizationScreen({Key key, this.organization}) : super(key: key);

  @override
  _OrganizationScreenState createState() => _OrganizationScreenState();
}

class _OrganizationScreenState extends State<OrganizationScreen> {
  var client = http.Client();


  Map<String,dynamic> organization;

  void addFavorite() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');

    final response = await client.post(
        Uri.parse('$endpoint/favorite/add/${this.widget.organization['id']}'),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        });

    final data = jsonDecode(response.body);


    if(response.statusCode == 201){
      SweetAlert.show(context, subtitle: "${data['message']}",style:SweetAlertStyle.success);

    }else{
      SweetAlert.show(context, subtitle: "${data['message']}",style:SweetAlertStyle.error);

    }
    print(data);
    setState(() {

      organization = data['favorites'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    organization = this.widget.organization;
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${organization['name']}',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 20),
                height: 232,
                width: width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            '$fileEndPoint${organization['image']}'),
                        fit: BoxFit.cover)),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            '${organization['name']}',
                            style: GoogleFonts.roboto(
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.star,
                              color: organization['rating'] > 0
                                  ? Color(0xffF2994A)
                                  : Colors.grey,
                            ),
                            Icon(
                              Icons.star,
                              color: organization['rating'] > 1
                                  ? Color(0xffF2994A)
                                  : Colors.grey,
                            ),
                            Icon(
                              Icons.star,
                              color: organization['rating'] > 2
                                  ? Color(0xffF2994A)
                                  : Colors.grey,
                            ),
                            Icon(
                              Icons.star,
                              color: organization['rating'] > 3
                                  ? Color(0xffF2994A)
                                  : Colors.grey,
                            ),
                            Icon(
                              Icons.star,
                              color: organization['rating'] > 4
                                  ? Color(0xffF2994A)
                                  : Colors.grey,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 15),
                              child: Text(
                                '${organization['rating']}',
                                style: GoogleFonts.roboto(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey),
                              ),
                            ),
                          ],
                        ),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                    GestureDetector(
                      onTap: () => addFavorite(),
                      child: Container(
                        padding: EdgeInsets.all(17),
                        decoration: BoxDecoration(
                            color: Color(0xffEBF3F4),
                            borderRadius: BorderRadius.circular(15)),
                        child: Icon(
                          organization['favorite'] ? Icons.favorite : Icons.favorite_border,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 32),
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(
                        '${organization['description']}',
                        style: GoogleFonts.roboto(
                            fontSize: 16, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 12, bottom: 10),
                      child: Row(
                        children: [
                          Icon(
                            Icons.place_outlined,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                '${organization['address']}',
                                style: GoogleFonts.roboto(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.access_time,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                '9.00 - 01.00',
                                style: GoogleFonts.roboto(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40, bottom: 48),
                      child: ButtonTheme(
                        minWidth: 264,
                        height: 48,
                        child: RaisedButton(
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChooseDateTimeScreen(
                                        organizationId:
                                            organization['id'],
                                      ))),
                          color: Color(0xff00A6CA),
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          child: Text(
                            'Перейти к брони',
                            style: GoogleFonts.roboto(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
