import 'package:btime/auth/Login.dart';
import 'package:btime/main.dart';
import 'package:btime/main/reserve/Organizations.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class CategoryItem extends StatelessWidget {
  CategoryItem(this.category);

  Map<String,dynamic> category;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>OrganizationsScreen(categoryId: category['id'],categoryName: category['name'],))),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top:20,bottom: 10),
            width: 130,
            height: 130,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage('$fileEndPoint${category['image']}'),
                  fit: BoxFit.cover

              ),
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          Container(
            child: Text('${category['name']}',style: GoogleFonts.roboto(
                fontSize: 16,
                fontWeight: FontWeight.w400
            ),),
          )
        ],
      ),
    );

  }
}
