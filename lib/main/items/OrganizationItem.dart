import 'package:btime/main/reserve/Organization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OrganizationItem extends StatelessWidget {

  OrganizationItem(this.organization);

  Map<String,dynamic> organization;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>OrganizationScreen(organization:organization))),
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        padding: EdgeInsets.only(
          top: 24,
          left: 20,
          bottom: 20,
          right: 20
        ),
        decoration: BoxDecoration(
          color: Color(0xffEBF3F4),
          borderRadius: BorderRadius.circular(15),

        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 5),
              child: Text('${organization['name']}',style: GoogleFonts.openSans(
                fontSize: 18,
                fontWeight: FontWeight.w600
              ),),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12),
              child: Row(
                children: [
                  Icon(Icons.star,color:organization['rating'] > 0? Color(0xffF2994A) : Colors.grey,),
                  Icon(Icons.star,color:organization['rating'] > 1? Color(0xffF2994A) : Colors.grey,),
                  Icon(Icons.star,color:organization['rating'] > 2? Color(0xffF2994A) : Colors.grey,),
                  Icon(Icons.star,color:organization['rating'] > 3? Color(0xffF2994A) : Colors.grey,),
                  Icon(Icons.star,color:organization['rating'] > 4? Color(0xffF2994A) : Colors.grey,),

                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Text('${organization['rating']}',style: GoogleFonts.roboto(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),),
                  ),

                ],
              ),
            ),
            Container(
              child: Text('${organization['address']}',style: GoogleFonts.roboto(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Colors.grey
              ),),
            )
          ],
        ),

      ),
    );
  }
}
