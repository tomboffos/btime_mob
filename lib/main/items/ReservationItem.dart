import 'package:btime/main/reserve/Reserve.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ReservationItem extends StatelessWidget {
  ReservationItem(this.reserve);

  Map<String, dynamic> reserve;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ReserveScreen(
                    reserve: reserve,
                  ))),
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        padding: EdgeInsets.only(top: 24, left: 20, bottom: 20, right: 20),
        decoration: BoxDecoration(
          color: Color(0xffEBF3F4),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 5),
              child: Text(
                '${reserve['organization']['name']}',
                style: GoogleFonts.openSans(
                    fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      '${DateTime.parse(reserve['date']).day.toString().length != 1 ? DateTime.parse(reserve['date']).day : '0${DateTime.parse(reserve['date']).day}'}'
                          '.${DateTime.parse(reserve['date']).month.toString().length != 1 ? DateTime.parse(reserve['date']).month : '0${DateTime.parse(reserve['date']).month}'}.${DateTime.parse(reserve['date']).year} ${reserve['start_time']}',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Text(
                '${reserve['organization']['address']}',
                style: GoogleFonts.roboto(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey),
              ),
            )
          ],
        ),
      ),
    );
  }
}
