import 'dart:convert';

import 'package:btime/auth/Login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../profile/update.dart';

import '../../main.dart';

class ProfileIndex extends StatefulWidget {
  @override
  _ProfileIndexState createState() => _ProfileIndexState();
}

class _ProfileIndexState extends State<ProfileIndex> {
  var client = http.Client();

  Map<String,dynamic> user;
  void getUser()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token =  prefs.get('token');

    final response = await client.get(Uri.parse('$endpoint/user'), headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"
    });

    if(response.statusCode == 200){
      final data = jsonDecode(response.body);
      setState(() {
        user = data['user'];
      });
    }

  }
  @override
  void initState() {
    // TODO: implement initState
    getUser();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Профиль',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
        actions: [
          GestureDetector(
            onTap: ()=>pushNewScreen(context, screen: UserUpdate()),
            child: Container(
              margin: EdgeInsets.only(right: 20),
              child: Icon(
                Icons.settings,
                size: 30,
                color: Theme.of(context).secondaryHeaderColor,
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: ()async {getUser();return true;},
        child: SingleChildScrollView(
          child: GestureDetector(
            onTap: (){
              if(user == null){
                getUser();
              }
            },
            child: Container(
              height: height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/back2.jpg'),
                      fit: BoxFit.fitHeight,alignment: Alignment.bottomCenter)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 76),
                    alignment: Alignment.center,
                    child: CircleAvatar(
                      foregroundImage: NetworkImage('${user != null ? fileEndPoint+user['avatar'].toString().replaceAll('public/', '') : 'Без имени'}'),
                      backgroundColor: Color(0xffEBF3F4),
                      radius: 82,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12),
                    child: Text(
                      '${user != null ? user['name'] : 'Без имени'}',
                      style: GoogleFonts.roboto(
                          fontSize: 20, fontWeight: FontWeight.w400,color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 140),
                    child: RaisedButton(
                      onPressed: () async{
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.remove('token');

                        pushNewScreen(
                          context,
                          screen: Login(),
                          withNavBar: false, // OPTIONAL VALUE. True by default.
                          pageTransitionAnimation:
                              PageTransitionAnimation.cupertino,
                        );
                      },
                      color: Color(0xffEBF3F4),
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Text(
                        'Выйти',
                        style: GoogleFonts.roboto(
                            fontSize: 14, fontWeight: FontWeight.w400),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
