import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:btime/auth/Login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

import '../../main.dart';

class UserUpdate extends StatefulWidget {
  @override
  _UserUpdateState createState() => _UserUpdateState();
}

class _UserUpdateState extends State<UserUpdate> {
  var client = http.Client();
  bool openImageChoose = false;
  TextEditingController _phone =
      MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController _password = TextEditingController();
  TextEditingController _name = TextEditingController();
  TextEditingController _city = TextEditingController();
  List<dynamic> cities = [];

  void getCities() async {
    final response = await client.get(Uri.parse('$endpoint/city'),
        headers: {"Accept": "application/json"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        final body = jsonDecode(response.body);
        cities = body['cities'];
      });
    }
  }

  Map<String, dynamic> user;

  void getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');

    final response = await client.get(Uri.parse('$endpoint/user'), headers: {
      "Accept": "application/json",
      "Authorization": "Bearer $token"
    });

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        user = data['user'];
        print(user);
        _phone.text = user['phone'];
        _name.text = user['name'];
        _city.text = user['city_id'].toString();
      });
    }
  }

  File _image;
  final picker = ImagePicker();

  Future chooseImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }
  Future chooseFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }

  void update() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final token = prefs.get('token');
    if (_password.text.length == 4 || _password.text.length == 0) {

      if(_image != null){
        var request = http.MultipartRequest('POST',Uri.parse('$endpoint/user/update'));
        request.files.add(http.MultipartFile('avatar',_image.readAsBytes().asStream(),_image.lengthSync(),filename: '${DateTime.now().toString()}-avatar'));
        request.fields.addAll({
          "phone": _phone.text,
          "password": _password.text,
          "name": _name.text,
          "city_id": _city.text,
        });
        request.headers.addAll({
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        });

        final response = await request.send();
        final body = await response.stream.bytesToString();
        print(body);
        setState(() {
          user = null;
        });
        getUser();
        if (response.statusCode == 200) {
          SweetAlert.show(context,
              subtitle: "Ваш профиль удачно обновлен",
              style: SweetAlertStyle.success);
        }
      }else{
        final response =
        await client.post(Uri.parse('$endpoint/user/update'), body: {
          "phone": _phone.text,
          "password": _password.text,
          "name": _name.text,
          "city_id": _city.text,
        }, headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        });
        if (response.statusCode == 200) {
          final data = jsonDecode(response.body);
          setState(() {
            user = null;
          });
          getUser();
          SweetAlert.show(context,
              subtitle: "Ваш профиль удачно обновлен",
              style: SweetAlertStyle.success);
        } else {
          print(response.body);
        }

      }
    } else {
      SweetAlert.show(context,
          subtitle: "Пароль должен состоять из 4 симолов",
          style: SweetAlertStyle.error);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getCities();
    getUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Обновление профиля'),
      ),
      body: user != null
          ? SingleChildScrollView(
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                        AwesomeDialog(
                          context: context,
                          animType: AnimType.SCALE,
                          headerAnimationLoop: false,
                          useRootNavigator: true,
                          dialogType: DialogType.NO_HEADER,
                          body: Column(
                            children: [
                              RaisedButton(
                                onPressed: ()async {
                                  await chooseFromGallery();
                                  Navigator.of(context, rootNavigator: true)?.pop();

                                },
                                child: Text('Выбрать из галереи',style: GoogleFonts.openSans(
                                    color: Colors.white
                                ),),
                                color: Theme.of(context).secondaryHeaderColor,
                              ),
                              RaisedButton(
                                onPressed: () async{
                                  await chooseImage();
                                  Navigator.of(context, rootNavigator: true)?.pop();

                                },
                                child: Text('Снять фото',style:  GoogleFonts.openSans(
                                    color: Colors.white
                                ),),
                                color: Theme.of(context).secondaryHeaderColor,
                              ),
                              _image != null ? RaisedButton(
                                onPressed: () {
                                  setState(() {
                                    _image = null;
                                  });
                                  Navigator.of(context, rootNavigator: true)?.pop();

                                },
                                child: Text('Удалить фото'),
                              ):SizedBox.shrink()
                            ],
                          ),
                          title: 'This is Ignored',
                          desc: 'This is also Ignored',
                          onDissmissCallback: ()=>{},
                          showCloseIcon: true,

                        )..show();

                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: 20, bottom: openImageChoose ? 0 : 20),
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        foregroundImage: _image != null
                            ? FileImage(_image)
                            : NetworkImage(
                                '${user != null ? fileEndPoint + user['avatar'].toString().replaceAll('public/', '') : ''}'),
                        backgroundColor: Color(0xffEBF3F4),
                        radius: 52,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: TextField(
                            controller: _name,
                            decoration: InputDecoration(
                              hintText: 'Ваше имя',
                              prefixIcon: Icon(
                                Icons.account_circle_outlined,
                                color: Theme.of(context).secondaryHeaderColor,
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: TextField(
                            controller: _phone,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              hintText: 'Ваш телефон',
                              prefixIcon: Icon(
                                Icons.phone,
                                color: Theme.of(context).secondaryHeaderColor,
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => {
                            if (cities.length == 0) {getCities()}
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 10),
                            child: DropdownButtonFormField(
                              decoration: InputDecoration(
                                hintText: 'Ваш город',
                                prefixIcon: Icon(
                                  Icons.location_city,
                                  color: Theme.of(context).secondaryHeaderColor,
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(40),
                                    borderSide: BorderSide(color: Colors.grey)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(40),
                                    borderSide: BorderSide(color: Colors.grey)),
                              ),
                              onChanged: (value) {
                                _city.text = value.toString();
                              },
                              items: cities.map((city) {
                                return DropdownMenuItem(
                                  child: Text(city['name']),
                                  value: city['id'],
                                );
                              }).toList(),
                              value: user != null ? user['city_id'] : '',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: TextField(
                            controller: _password,
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'Ваш пароль',
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Theme.of(context).secondaryHeaderColor,
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide(color: Colors.grey)),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    margin: EdgeInsets.only(top: 32),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RaisedButton(
                          onPressed: ()async{
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.remove('token');

                            pushNewScreen(
                              context,
                              screen: Login(),
                              withNavBar: false, // OPTIONAL VALUE. True by default.
                              pageTransitionAnimation:
                              PageTransitionAnimation.cupertino,
                            );
                          },
                          color: Color(0xffEBF3F4),
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          child: Text(
                            'Выйти',
                            style: GoogleFonts.roboto(
                                fontSize: 14, fontWeight: FontWeight.w400,),
                          ),
                        ),

                        RaisedButton(
                          onPressed: () => update(),
                          color: Theme.of(context).secondaryHeaderColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          child: Text(
                            'Обновить профиль',
                            style: GoogleFonts.roboto(
                                fontSize: 14, fontWeight: FontWeight.w400,color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
