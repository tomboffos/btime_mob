import 'dart:convert';

import 'package:btime/main/items/OrganizationItem.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

class FavoriteIndex extends StatefulWidget {
  @override
  _FavoriteIndexState createState() => _FavoriteIndexState();
}

class _FavoriteIndexState extends State<FavoriteIndex> {
  List<dynamic> organizations = [];
  var client = http.Client();

  void getOrganizations() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');
    final response = await client.get(Uri.parse('$endpoint/favorite'),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        });

    print(response.body);
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        organizations = body['favorites'];
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getOrganizations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Избранное',
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w400),
        ),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: organizations.length > 0 ? RefreshIndicator(

          onRefresh: () async {
            await getOrganizations();
          },
          child: Container(
            margin: EdgeInsets.all(20),
            child: ListView.builder(itemBuilder: (context,index)=>OrganizationItem(organizations[index]),itemCount: organizations.length,),
          ),
        ) : RefreshIndicator(
          onRefresh: () async {
            await getOrganizations();
          },
          child: SingleChildScrollView(
            child: GestureDetector(
              onTap: () => getOrganizations(),
              child: Container(
                height: 550,
                child: Center(
                  child: Text('Список пуст'),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
