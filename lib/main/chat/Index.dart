import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ChatIndex extends StatefulWidget {
  @override
  _ChatIndexState createState() => _ChatIndexState();
}

class _ChatIndexState extends State<ChatIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Чат',style: GoogleFonts.roboto(
            fontSize: 20,
            fontWeight: FontWeight.w400
        ),),

      ),

      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Text('У вас пока нет сообщении'),

            ),
          ],
        ),
      ),
    );
  }
}
