import 'package:btime/main/chat/Index.dart';
import 'package:btime/main/favorite/Index.dart';
import 'package:btime/main/profile/Index.dart';
import 'package:btime/main/reserve/Index.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class MainNavigation extends StatefulWidget {
  @override
  _MainNavigationState createState() => _MainNavigationState();
}

class _MainNavigationState extends State<MainNavigation> {
  List<Widget> _buildScreens() {
    return [
      ProfileIndex(),
      ReserveIndex(),
      FavoriteIndex(),
      ChatIndex(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItem() {
    return [
      PersistentBottomNavBarItem(
          icon: Icon(Icons.account_circle),
          title: ('Главное'),
          activeColorPrimary: Color(0xff00A6CA),
          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.access_time),
          title: ('Профиль'),
          activeColorPrimary: Color(0xff00A6CA),

          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.favorite_border),
          title: ('Профиль'),
          activeColorPrimary: Color(0xff00A6CA),

          inactiveColorPrimary: Colors.grey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.message),
          title: ('Профиль'),
          activeColorPrimary: Color(0xff00A6CA),
          inactiveColorPrimary: Colors.grey),
    ];
  }

  int _viewIndex;

  PersistentTabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    _viewIndex = 0;
    _controller = PersistentTabController(initialIndex: 0);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItem(),
      backgroundColor: Colors.white,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      onItemSelected: (int) {
        setState(
            () {}); // This is required to update the nav bar if Android back button is pressed
      },
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows: true,
      decoration: NavBarDecoration(borderRadius: BorderRadius.circular(0)),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      navBarStyle: NavBarStyle.style12,
    );
  }

  void _changeView(int index) {
    setState(() {
      _viewIndex = index;
    });
  }
}
