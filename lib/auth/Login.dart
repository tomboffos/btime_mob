import 'dart:convert';

import 'package:btime/auth/Registration.dart';
import 'package:btime/main.dart';
import 'package:btime/main/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  TextEditingController _phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');

  TextEditingController _password = TextEditingController();
  var client = http.Client();

  void login() async {
//    var client = createHttpClient();

    if (_password.text.length == 4) {
      final response = await client.post(Uri.parse('$endpoint/login'),
          body: {"phone": _phone.text, "password": _password.text},
          headers: {"Accept": "application/json"});

      print(response.statusCode);
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', data['token']);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>MainNavigation()), (route) => false);

      } else {
        SweetAlert.show(context,
            subtitle: "Не правильный пароль или номер телефона",
            style: SweetAlertStyle.error);
      }
    } else {
      SweetAlert.show(context,
          subtitle: "Пароль должен состоять из 4 симолов",
          style: SweetAlertStyle.error);
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 50, bottom: 0),
              child: Image.asset(
                'assets/images/logo.png',
                width: 75,
                height: 75,
              ),
            ),
            Container(
              child: Image.asset(
                'assets/images/images.png',
                width: 500,
                height: 150,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              margin: EdgeInsets.only(bottom: 50),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: TextField(
                      keyboardType: TextInputType.phone,
                      controller: _phone,
                      decoration: InputDecoration(
                        hintText: 'Ваш телефон',
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: TextField(
                      obscureText: true,
                      controller: _password,
                      decoration: InputDecoration(
                        hintText: 'Ваш пароль',
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              margin: EdgeInsets.only(top: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Registration())),
                    color: Color(0xffEBF3F4),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      'Регистрация',
                      style: GoogleFonts.roboto(
                          fontSize: 14, fontWeight: FontWeight.w400),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () => login(),
                    color: Color(0xff00A6CA),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      'Войти',
                      style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
