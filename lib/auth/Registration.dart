import 'dart:convert';

import 'package:btime/auth/Login.dart';
import 'package:btime/main/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

import '../main.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});
  var client = http.Client();

  TextEditingController _phone =
      MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController _password = TextEditingController();
  TextEditingController _name = TextEditingController();
  TextEditingController _city = TextEditingController();

  List<dynamic> cities = [];

  void getCities() async {
    final response = await client.get(Uri.parse('$endpoint/city'),
        headers: {"Accept": "application/json"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        final body = jsonDecode(response.body);
        cities = body['cities'];
      });
    }
  }

  void register() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (_password.text.length == 4) {
      final response =
          await client.post(Uri.parse('$endpoint/register'), body: {
        "phone": _phone.text,
        "password": _password.text,
        "name": _name.text,
        "city_id": _city.text
      }, headers: {
        "Accept": "application/json"
      });

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        prefs.setString('token', data['token']);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>MainNavigation()), (route) => false);

      } else {
        print(response.body);
      }
    } else {
      SweetAlert.show(context,
          subtitle: "Пароль должен состоять из 4 симолов",
          style: SweetAlertStyle.error);
    }
  }

  bool loading = false;

  void getUser() async {
    setState(() {
      loading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    print(token);
    if (token != null) {
      final response = await client.get(Uri.parse('$endpoint/user'), headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token"
      });

      if (response.statusCode == 200) {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>MainNavigation()), (route) => false);
      }
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getCities();
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: loading == false ? SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 50, bottom: 0),
              child: Text('Reserved',style: GoogleFonts.roboto(
                fontSize: 19,
                fontWeight: FontWeight.w500
              ),)
            ),
            Container(
              child: Image.asset(
                'assets/images/images.png',
                width: 360,
                height: 150,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: TextField(
                      controller: _name,
                      decoration: InputDecoration(
                        hintText: 'Ваше имя',
                        prefixIcon: Icon(
                          Icons.account_circle_outlined,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: TextField(
                      controller: _phone,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        hintText: 'Ваш телефон',
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => {
                      if (cities.length == 0) {getCities()}
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          hintText: 'Ваш город',
                          prefixIcon: Icon(
                            Icons.location_city,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide(color: Colors.grey)),
                        ),
                        onChanged: (value) {
                          _city.text = value.toString();
                        },
                        items: cities.map((city) {
                          return DropdownMenuItem(
                            child: Text(city['name']),
                            value: city['id'],
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: TextField(
                      controller: _password,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: 'Ваш пароль',
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide(color: Colors.grey)),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              margin: EdgeInsets.only(top: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    onPressed: () => register(),
                    color: Color(0xffEBF3F4),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      'Регистрация',
                      style: GoogleFonts.roboto(
                          fontSize: 14, fontWeight: FontWeight.w400),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login())),
                    color: Color(0xff00A6CA),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      'Войти',
                      style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ) : Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
